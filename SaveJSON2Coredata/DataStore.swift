//
//  DataStore.swift
//  SaveJSON2Coredata
//
//  Created by EPITADMBP04 on 5/16/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation
import CoreData

class DataStore: NSObject {
    
    //MARK:- Created objects to use the respective class methods.
    let persistence = PersistenceService.shared
    let networking = NetworkingService.shared
    
    private override init() {
        super.init()
    }
    //MARK:- Singleton object for DataStore
    static let shared = DataStore()
    
    //MARK:- Making Network request call with the EndPoint URL to get the users.
    func requestUsers(completion: @escaping ([User]) -> Void) {
        // Go out to the Internet
        networking.request("https://kiloloco.com/api/users") { [weak self] (result) in
            switch result {
            //MARK:- Used JSONSerialization to recover the original object from the byte string
            case .success(let data):
                do {
                    guard let jsonArray = try JSONSerialization.jsonObject(with: data, options: []) as? [[String:Any]] else { return }
                    
                    jsonArray.forEach {
                        guard
                            let strongSelf = self,
                            let name = $0["name"] as? String,
                            let age = $0["age"] as? Int16,
                            let id = $0["id"] as? Int16
                            else { return }
                        //MARK:- Assiging the retreived data objects to the persistence context, data eventually goes to the "PersistentContainer"
                        let user = User(context: strongSelf.persistence.context)
                        user.name = name
                        user.age = age
                        user.id = id
                    }
                    
                    DispatchQueue.main.async {
                        self?.persistence.saveContext() {
                            self?.persistence.fetch(User.self, completion: { (objects) in
                                completion(objects)
                            })
                        }
                    }
                } catch {
                    print(error)
                }
            case .failure(let error): print(error)
            }
        }
    }
}
