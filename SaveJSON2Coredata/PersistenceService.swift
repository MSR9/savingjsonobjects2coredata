//
//  PersistenceService.swift
//  SaveJSON2Coredata
//
//  Created by EPITADMBP04 on 5/15/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class PersistenceService {
    
    //MARK:- Singleton object for the "PersistenceService"
    fileprivate init() {}
    static let shared = PersistenceService()
    
    var context: NSManagedObjectContext { return persistentContainer.viewContext }
    
    //MARK:- Getting the PersistentContainer to store the users into it.
    lazy var persistentContainer: NSPersistentContainer = {
           let container = NSPersistentContainer(name: "SaveJSON2Coredata")
           container.loadPersistentStores(completionHandler: { (storeDescription, error) in
               if let error = error as NSError? {
                   fatalError("Unresolved error \(error), \(error.userInfo)")
               }
           })
           return container
       }()
    //MARK:- Storing the actual content which is users into the PersistentContainer.
    func saveContext (completion: @escaping () -> Void) {
           let context = persistentContainer.viewContext
           if context.hasChanges {
               do {
                   try context.save()
                completion()
                NotificationCenter.default.post(name: NSNotification.Name("PersistedDataUpdated"), object: nil)
                print("Saved Successfully")
               } catch {
                   let nserror = error as NSError
                   fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
               }
           }
       }
    //MARK:- Getting the Users back from the PersistentContainer 
    func fetch<T: NSManagedObject>(_ type: T.Type, completion: @escaping ([T]) -> Void) {
        let request = NSFetchRequest<T>(entityName: String(describing: type))
        do {
            let objects = try context.fetch(request)
            completion(objects)
        } catch {
            print(error)
            completion([])
        }
    }
}
