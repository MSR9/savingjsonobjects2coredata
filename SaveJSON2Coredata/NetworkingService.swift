//
//  Persister.swift
//  SaveJSON2Coredata
//
//  Created by EPITADMBP04 on 5/15/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation
import UIKit

class NetworkingService {
 
    private init() {}
    
    //MARK:- Singleton Object for "NetworkingService"
    static let shared = NetworkingService()
    
    //MARK:- This Method will prepare the reuest and initiate the Network service request.
    func request(_ urlPath: String, completion: @escaping (Result<Data, NSError>) -> Void) {
        let url = URL(string: urlPath)!
        
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, _, error) in
            
                if let unwrappedData = data {
                completion(.success(unwrappedData))
                } else if let unwrappedError = error {
                    completion(.failure(unwrappedError as NSError))
                }
            }
            task.resume()
        }
   }










































