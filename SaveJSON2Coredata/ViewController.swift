//
//  ViewController.swift
//  SaveJSON2Coredata
//
//  Created by EPITADMBP04 on 5/15/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit
import Foundation 

class ViewController: UITableViewController {
    
    //MARK:- Created a DataStore object to access its methods.
    let store = DataStore.shared
    
    //MARK:- Saving Users here
    var users = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    //MARK:- Calling "requestUsers" method to get users list and reloading tableView to display results on screen.
        store.requestUsers() { [weak self] (users) in
            self?.users = users
            self?.tableView.reloadData()
        }
    }
    
    //MARK:- TABLEVIEW METHODS
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let user = users[indexPath.row]
        
    //MARK:- Assiging "userName" and "id" to the TableView Cell labels.
        cell.textLabel?.text = user.name + "  is \(user.age) years old"
        cell.detailTextLabel?.text = String(user.id)
        return cell
    }
}
