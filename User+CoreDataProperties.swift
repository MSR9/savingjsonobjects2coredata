//
//  User+CoreDataProperties.swift
//  SaveJSON2Coredata
//
//  Created by EPITADMBP04 on 5/15/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var age: Int16
    @NSManaged public var id: Int16
    @NSManaged public var name: String

}
